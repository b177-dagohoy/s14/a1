console.log("Hello, World");

let firstName = 'John';
console.log("First Name: " + firstName);

let lastName = 'Smith';
console.log("Last Name: " + lastName);

let age = 33;
console.log("Age: " + age);

let hobbies = ["Swimming", "Crafting", "Baking"];
console.log("Hobbies: ")
console.log(hobbies);

let workAddress = {
	houseNumber: 162,
	street: "Basswood",
	city: "Los Angeles",
	state: "California"
};
console.log("Work Address: ");
console.log(workAddress);

function printUserInfo(firstName, LastName, age, hobbies, workAddress){
	console.log(firstName + " " + lastName + " is " + age + " years of age.");
	console.log("This was printed inside of the function");
	console.log(hobbies);
	console.log("This was printed inside of the function");
	console.log(workAddress);
};

printUserInfo('John', 'Smith', 33, hobbies, workAddress);

function returnFunction(isMarried){
	return isMarried
}

let isMarried = returnFunction(true);
console.log("The value of isMarried is: " + isMarried);